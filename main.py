from logic.QrGenerator import QRGenerator
import os

#Constante para saber donde esta el root de este archivo .py
OUTPUT_DIR = os.getcwd()
#Instancia del generador de codigos QR
generator = QRGenerator(OUTPUT_DIR)
#Links dados por el cliente para crear codigos QR
#   -> La estructura de cada elemento es (link, nombre del codigo QR deseado)
URLCollection = [
    ("https://www.instagram.com/nubesinos/?hl=es-la", "nubesinos_instagram.png"),
    ("https://www.instagram.com/homechef_nubesin0s/?igshid=YmMyMTA2M2Y%3D", "nubesions_home_chef.png"),
    ("https://www.facebook.com/nubesinos", "nubesions_facebook.png"),
    ("https://www.facebook.com/ieicr", "ieicr_facebook.png")
]
#creamos los codigos QR para cada link
for urlObj in URLCollection:
    generator.generateQRCode(urlObj)