import qrcode

class QRGenerator():
    """
    This generator gets a url and file name to create a static QR Code    
    Inputs: 
            pSaveDir: The path where you want to save the QR Code
    Output: The file with the QR code
    Restrictions: No validations were made.
    """

    def __init__(self, pSaveDir : str) -> None:
        self.saveDir = pSaveDir

    def generateQRCode(self, pURLObj : str):
        """
        Main function for this code
        Inputs: 
            pURLOn: a tuple with (url, file name)
        Output: The file with the QR code
        Restrictions: No validations were made.

        """
        #Creates the image with the Url located at pURLObj tuple at index 0
        img = qrcode.make(pURLObj[0])
        #Opens a writable file with the image in the output direction from parameter and set the filename
        #located at pURLObj tuple at index 0
        f = open(self.saveDir + "/" + pURLObj[1], "wb")
        #Saves the image in the opened file
        img.save(f)
        f.close()
        print("QR Code Generated successfully at: ", self.saveDir)
        return img

    def getSaveDir(self) -> str:
        """
        Returns the path where QR Codes will be saved
        """
        return self.saveDir

    def setSaveDir(self, pSaveDir : str) -> None:
        self.saveDir = pSaveDir